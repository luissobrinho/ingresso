<?php

use App\Models\User;
use App\Services\UserService;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = app(UserService::class);

        if (!User::where('name', 'Admin')->first()) {
            $user = User::create([
                'name' => 'Admin',
                'email' => 'admin@e-ingresso.dev.br',
                'cpf' => '000.000.000-00',
                'password' => bcrypt('admin'),
            ]);

            $service->create($user, 'admin', [
                'roles' => 'admin',
                'bornDate' => '1990-01-01',
                'phone' => '(00) 00000-0000'
            ], false);
            $user->meta->update([
                'is_active' => true,
            ]);
        }

    }
}
