<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
			$table->boolean('status')->default(true);
			$table->string('nome');
			$table->integer('time_1_id')->unsigned();
			$table->integer('time_2_id')->unsigned();
			$table->integer('estadio_id')->unsigned();
			$table->decimal('quantidade_alta_valor', 10, 2);
			$table->integer('quantidade_alta');
			$table->integer('quantidade_baixa');
			$table->decimal('quantidade_baixa_valor', 10, 2);
			$table->integer('quantidade_especial');
			$table->decimal('quantidade_especial_valor', 10, 2);
			$table->date('data_evento');
			$table->text('descricao');
			$table->string('foto');

            $table->foreign('time_1_id')
                ->references('id')
                ->on('times')
                ->onDelete('cascade');
            $table->foreign('time_2_id')
                ->references('id')
                ->on('times')
                ->onDelete('cascade');
            $table->foreign('estadio_id')
                ->references('id')
                ->on('estadios')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
