<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_meta', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('depedente')->default(3);
            $table->string('phone')->nullable();
            $table->date('bornDate')->nullable();
            $table->string('photo')->nullable();
            $table->string('identidade')->nullable();
            $table->string('comprovante')->nullable();

            $table->boolean('is_active')->default(0);
            $table->string('activation_token')->nullable();

            $table->boolean('marketing')->default(0);
            $table->boolean('terms_and_cond')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_meta');
    }
}
