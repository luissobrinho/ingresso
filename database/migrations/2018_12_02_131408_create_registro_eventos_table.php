<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_eventos', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('evento_id')->unsigned();
			$table->integer('pessoa_id')->unsigned()->nullable();
			$table->decimal('valor', 10, 2)->nullable();
			$table->integer('assento')->nullable();
			$table->boolean('meia')->nullable();
			$table->string('uuid');
			$table->boolean('status')->default(0);
            $table->string('link_pagamento')->nullable();
            $table->string('code_pagamento')->nullable();
            $table->string('data_pagamento')->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('evento_id')
                ->references('id')
                ->on('eventos')
                ->onDelete('cascade');
            $table->foreign('pessoa_id')
                ->references('id')
                ->on('pessoas')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_eventos');
    }
}
