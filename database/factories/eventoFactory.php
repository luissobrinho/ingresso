<?php
/*
|--------------------------------------------------------------------------
| Evento Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Evento::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'nome' => '1',
		'quantidade_alta' => '1',
		'quantidade_baixa' => '1',
		'quantidade_especial' => '1',
		'valor' => '1',
		'descricao' => 'tempore adipisci tenetur nostrum',
		'foto' => 'ut',
    ];
});
