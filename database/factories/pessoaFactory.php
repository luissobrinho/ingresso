<?php
/*
|--------------------------------------------------------------------------
| Pessoa Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Pessoa::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'nome' => 'quo',
		'email' => 'quod',
		'cpf' => 'expedita',
    ];
});
