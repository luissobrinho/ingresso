<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TimeAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Time = factory(App\Models\Time::class)->make([
            'id' => '1',
		'nome' => 'voluptatem',

        ]);
        $this->TimeEdited = factory(App\Models\Time::class)->make([
            'id' => '1',
		'nome' => 'voluptatem',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'times');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('times');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'times/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'times', $this->Time->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('times/'.$this->Time->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'times', $this->Time->toArray());

        $response = $this->actor->call('GET', '/times/'.$this->Time->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('time');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'times', $this->Time->toArray());
        $response = $this->actor->call('PATCH', 'times/1', $this->TimeEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('times', $this->TimeEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'times', $this->Time->toArray());

        $response = $this->call('DELETE', 'times/'.$this->Time->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('times');
    }

}
