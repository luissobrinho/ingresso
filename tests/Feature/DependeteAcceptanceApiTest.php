<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DependeteAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Dependete = factory(App\Models\Dependete::class)->make([
            'id' => '1',
		'nome' => 'quasi',
		'email' => 'amet',
		'cpf' => 'perferendis',
		'user_id' => '1',

        ]);
        $this->DependeteEdited = factory(App\Models\Dependete::class)->make([
            'id' => '1',
		'nome' => 'quasi',
		'email' => 'amet',
		'cpf' => 'perferendis',
		'user_id' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/dependetes');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/dependetes', $this->Dependete->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/dependetes', $this->Dependete->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/dependetes/1', $this->DependeteEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('dependetes', $this->DependeteEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/dependetes', $this->Dependete->toArray());
        $response = $this->call('DELETE', 'api/v1/dependetes/'.$this->Dependete->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'dependete was deleted']);
    }

}
