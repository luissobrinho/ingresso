<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ListaNegraAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->ListaNegra = factory(App\Models\ListaNegra::class)->make([
            'id' => '1',
		'cpf' => 'vero',
		'user_id' => '1',

        ]);
        $this->ListaNegraEdited = factory(App\Models\ListaNegra::class)->make([
            'id' => '1',
		'cpf' => 'vero',
		'user_id' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/lista_negras');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/lista_negras', $this->ListaNegra->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/lista_negras', $this->ListaNegra->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/lista_negras/1', $this->ListaNegraEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('lista_negras', $this->ListaNegraEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/lista_negras', $this->ListaNegra->toArray());
        $response = $this->call('DELETE', 'api/v1/lista_negras/'.$this->ListaNegra->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'lista_negra was deleted']);
    }

}
