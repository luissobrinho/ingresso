<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EventoAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Evento = factory(App\Models\Evento::class)->make([
            'id' => '1',
		'nome' => '1',
		'quantidade_alta' => '1',
		'quantidade_baixa' => '1',
		'quantidade_especial' => '1',
		'valor' => '1',
		'descricao' => 'aperiam impedit doloremque possimus',
		'foto' => 'corrupti',

        ]);
        $this->EventoEdited = factory(App\Models\Evento::class)->make([
            'id' => '1',
		'nome' => '1',
		'quantidade_alta' => '1',
		'quantidade_baixa' => '1',
		'quantidade_especial' => '1',
		'valor' => '1',
		'descricao' => 'aperiam impedit doloremque possimus',
		'foto' => 'corrupti',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/eventos');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/eventos', $this->Evento->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/eventos', $this->Evento->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/eventos/1', $this->EventoEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('eventos', $this->EventoEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/eventos', $this->Evento->toArray());
        $response = $this->call('DELETE', 'api/v1/eventos/'.$this->Evento->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'evento was deleted']);
    }

}
