<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegistroEventoAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RegistroEvento = factory(App\Models\RegistroEvento::class)->make([
            'id' => '1',
		'user_id' => '1',
		'evento_id' => '1',
		'uuid' => 'voluptas',
		'status' => '1',
		'pessoa_id' => '1',

        ]);
        $this->RegistroEventoEdited = factory(App\Models\RegistroEvento::class)->make([
            'id' => '1',
		'user_id' => '1',
		'evento_id' => '1',
		'uuid' => 'voluptas',
		'status' => '1',
		'pessoa_id' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/registro_eventos');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/registro_eventos', $this->RegistroEvento->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/registro_eventos', $this->RegistroEvento->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/registro_eventos/1', $this->RegistroEventoEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('registro_eventos', $this->RegistroEventoEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/registro_eventos', $this->RegistroEvento->toArray());
        $response = $this->call('DELETE', 'api/v1/registro_eventos/'.$this->RegistroEvento->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'registro_evento was deleted']);
    }

}
