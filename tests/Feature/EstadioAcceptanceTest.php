<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EstadioAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Estadio = factory(App\Models\Estadio::class)->make([
            'id' => '1',
		'nome' => 'atque',

        ]);
        $this->EstadioEdited = factory(App\Models\Estadio::class)->make([
            'id' => '1',
		'nome' => 'atque',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'estadios');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('estadios');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'estadios/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'estadios', $this->Estadio->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('estadios/'.$this->Estadio->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'estadios', $this->Estadio->toArray());

        $response = $this->actor->call('GET', '/estadios/'.$this->Estadio->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('estadio');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'estadios', $this->Estadio->toArray());
        $response = $this->actor->call('PATCH', 'estadios/1', $this->EstadioEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('estadios', $this->EstadioEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'estadios', $this->Estadio->toArray());

        $response = $this->call('DELETE', 'estadios/'.$this->Estadio->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('estadios');
    }

}
