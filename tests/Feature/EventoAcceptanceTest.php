<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EventoAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Evento = factory(App\Models\Evento::class)->make([
            'id' => '1',
		'nome' => '1',
		'quantidade_alta' => '1',
		'quantidade_baixa' => '1',
		'quantidade_especial' => '1',
		'valor' => '1',
		'descricao' => 'voluptatibus minima illo voluptatem',
		'foto' => 'libero',

        ]);
        $this->EventoEdited = factory(App\Models\Evento::class)->make([
            'id' => '1',
		'nome' => '1',
		'quantidade_alta' => '1',
		'quantidade_baixa' => '1',
		'quantidade_especial' => '1',
		'valor' => '1',
		'descricao' => 'voluptatibus minima illo voluptatem',
		'foto' => 'libero',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'eventos');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('eventos');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'eventos/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'eventos', $this->Evento->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('eventos/'.$this->Evento->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'eventos', $this->Evento->toArray());

        $response = $this->actor->call('GET', '/eventos/'.$this->Evento->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('evento');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'eventos', $this->Evento->toArray());
        $response = $this->actor->call('PATCH', 'eventos/1', $this->EventoEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('eventos', $this->EventoEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'eventos', $this->Evento->toArray());

        $response = $this->call('DELETE', 'eventos/'.$this->Evento->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('eventos');
    }

}
