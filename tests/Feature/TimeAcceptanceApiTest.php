<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TimeAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Time = factory(App\Models\Time::class)->make([
            'id' => '1',
		'nome' => 'vel',

        ]);
        $this->TimeEdited = factory(App\Models\Time::class)->make([
            'id' => '1',
		'nome' => 'vel',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/times');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/times', $this->Time->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/times', $this->Time->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/times/1', $this->TimeEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('times', $this->TimeEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/times', $this->Time->toArray());
        $response = $this->call('DELETE', 'api/v1/times/'.$this->Time->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'time was deleted']);
    }

}
