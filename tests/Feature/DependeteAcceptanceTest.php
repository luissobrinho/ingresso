<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DependeteAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Dependete = factory(App\Models\Dependete::class)->make([
            'id' => '1',
		'nome' => 'aut',
		'email' => 'soluta',
		'cpf' => 'et',
		'user_id' => '1',

        ]);
        $this->DependeteEdited = factory(App\Models\Dependete::class)->make([
            'id' => '1',
		'nome' => 'aut',
		'email' => 'soluta',
		'cpf' => 'et',
		'user_id' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'dependetes');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('dependetes');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'dependetes/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'dependetes', $this->Dependete->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('dependetes/'.$this->Dependete->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'dependetes', $this->Dependete->toArray());

        $response = $this->actor->call('GET', '/dependetes/'.$this->Dependete->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('dependete');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'dependetes', $this->Dependete->toArray());
        $response = $this->actor->call('PATCH', 'dependetes/1', $this->DependeteEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('dependetes', $this->DependeteEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'dependetes', $this->Dependete->toArray());

        $response = $this->call('DELETE', 'dependetes/'.$this->Dependete->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('dependetes');
    }

}
