<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ListaNegraAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->ListaNegra = factory(App\Models\ListaNegra::class)->make([
            'id' => '1',
		'cpf' => 'laborum',
		'user_id' => '1',

        ]);
        $this->ListaNegraEdited = factory(App\Models\ListaNegra::class)->make([
            'id' => '1',
		'cpf' => 'laborum',
		'user_id' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'lista_negras');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('lista_negras');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'lista_negras/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'lista_negras', $this->ListaNegra->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('lista_negras/'.$this->ListaNegra->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'lista_negras', $this->ListaNegra->toArray());

        $response = $this->actor->call('GET', '/lista_negras/'.$this->ListaNegra->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('lista_negra');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'lista_negras', $this->ListaNegra->toArray());
        $response = $this->actor->call('PATCH', 'lista_negras/1', $this->ListaNegraEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('lista_negras', $this->ListaNegraEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'lista_negras', $this->ListaNegra->toArray());

        $response = $this->call('DELETE', 'lista_negras/'.$this->ListaNegra->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('lista_negras');
    }

}
