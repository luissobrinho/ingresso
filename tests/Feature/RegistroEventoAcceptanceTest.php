<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegistroEventoAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RegistroEvento = factory(App\Models\RegistroEvento::class)->make([
            'id' => '1',
		'user_id' => '1',
		'evento_id' => '1',
		'uuid' => 'voluptate',
		'status' => '1',
		'pessoa_id' => '1',

        ]);
        $this->RegistroEventoEdited = factory(App\Models\RegistroEvento::class)->make([
            'id' => '1',
		'user_id' => '1',
		'evento_id' => '1',
		'uuid' => 'voluptate',
		'status' => '1',
		'pessoa_id' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'registro_eventos');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('registro_eventos');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'registro_eventos/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'registro_eventos', $this->RegistroEvento->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('registro_eventos/'.$this->RegistroEvento->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'registro_eventos', $this->RegistroEvento->toArray());

        $response = $this->actor->call('GET', '/registro_eventos/'.$this->RegistroEvento->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('registro_evento');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'registro_eventos', $this->RegistroEvento->toArray());
        $response = $this->actor->call('PATCH', 'registro_eventos/1', $this->RegistroEventoEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('registro_eventos', $this->RegistroEventoEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'registro_eventos', $this->RegistroEvento->toArray());

        $response = $this->call('DELETE', 'registro_eventos/'.$this->RegistroEvento->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('registro_eventos');
    }

}
