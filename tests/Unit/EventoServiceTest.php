<?php

use Tests\TestCase;
use App\Services\EventoService;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class EventoServiceTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->service = $this->app->make(EventoService::class);
        $this->originalArray = [
            'id' => '1',
		'nome' => '1',
		'quantidade_alta' => '1',
		'quantidade_baixa' => '1',
		'quantidade_especial' => '1',
		'valor' => '1',
		'descricao' => 'laborum aut eos voluptas',
		'foto' => 'delectus',

        ];
        $this->editedArray = [
            'id' => '1',
		'nome' => '1',
		'quantidade_alta' => '1',
		'quantidade_baixa' => '1',
		'quantidade_especial' => '1',
		'valor' => '1',
		'descricao' => 'laborum aut eos voluptas',
		'foto' => 'delectus',

        ];
        $this->searchTerm = '';
    }

    public function testAll()
    {
        $response = $this->service->all();
        $this->assertEquals(get_class($response), 'Illuminate\Database\Eloquent\Collection');
        $this->assertTrue(is_array($response->toArray()));
        $this->assertEquals(0, count($response->toArray()));
    }

    public function testPaginated()
    {
        $response = $this->service->paginated(25);
        $this->assertEquals(get_class($response), 'Illuminate\Pagination\LengthAwarePaginator');
        $this->assertEquals(0, $response->total());
    }

    public function testSearch()
    {
        $response = $this->service->search($this->searchTerm, 25);
        $this->assertEquals(get_class($response), 'Illuminate\Pagination\LengthAwarePaginator');
        $this->assertEquals(0, $response->total());
    }

    public function testCreate()
    {
        $response = $this->service->create($this->originalArray);
        $this->assertEquals(get_class($response), 'App\Models\Evento');
        $this->assertEquals(1, $response->id);
    }

    public function testFind()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->find($item->id);
        $this->assertEquals($item->id, $response->id);
    }

    public function testUpdate()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->update($item->id, $this->editedArray);

        $this->assertDatabaseHas('eventos', $this->editedArray);
    }

    public function testDestroy()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->destroy($item->id);
        $this->assertTrue($response);
    }
}
