<?php

namespace App\Services;

use App\Models\Evento;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class EventoService
{
    /**
     * Service Model
     *
     * @var Model
     */
    public $model;

    /**
     * Pagination
     *
     * @var integer
     */
    public $pagination;

    /**
     * Service Constructor
     *
     * @param Evento $evento
     */
    public function __construct(Evento $evento)
    {
        $this->model = $evento;
        $this->pagination = 25;
    }

    /**
     * All Model Items
     *
     * @return array
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Paginated items
     *
     * @return LengthAwarePaginator
     */
    public function paginated()
    {
        return $this->model->paginate($this->pagination);
    }

    /**
     * Search the model
     *
     * @param  mixed $payload
     * @return LengthAwarePaginator
     */
    public function search($payload)
    {
        $query = $this->model->orderBy('created_at', 'desc');
        $query->where($this->model->primaryKey, 'LIKE', '%'.$payload.'%');

        $columns = Schema::getColumnListing('eventos');

        foreach ($columns as $attribute) {
            $query->orWhere($attribute, 'LIKE', '%'.$payload.'%');
        };

        return $query->paginate($this->pagination)->appends([
            'search' => $payload
        ]);
    }

    /**
     * Create the model item
     *
     * @param  array $payload
     * @return Model
     * @throws \Exception
     */
    public function create($payload)
    {
        if(key_exists('arquivo', $payload)) {
            $fileName = date('YmdHis') . '.' . $payload['arquivo']->extension();
            $path = 'files'.DIRECTORY_SEPARATOR.'eventos'.DIRECTORY_SEPARATOR.date('Y').DIRECTORY_SEPARATOR.date('m');

            if($payload['arquivo']->move($path, $fileName)) {
                $payload['foto'] = str_replace('\\', '/', $path).'/'.$fileName;
            } else {
                throw new \Exception('Erro no envio do arquivo');
            }
        }

        return $this->model->create($payload);
    }

    /**
     * Find Model by ID
     *
     * @param  integer $id
     * @return Model
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * Model update
     *
     * @param  integer $id
     * @param  array $payload
     * @return Model
     */
    public function update($id, $payload)
    {
        if(key_exists('arquivo', $payload)) {
            $fileName = date('YmdHis') . '.' . $payload['arquivo']->extension();
            $path = 'files'.DIRECTORY_SEPARATOR.'eventos'.DIRECTORY_SEPARATOR.date('Y').DIRECTORY_SEPARATOR.date('m');

            if($payload['arquivo']->move($path, $fileName)) {
                $payload['foto'] = str_replace('\\', '/', $path).'/'.$fileName;
            } else {
                throw new \Exception('Erro no envio do arquivo');
            }
        }

        return $this->find($id)->update($payload);
    }

    /**
     * Destroy the model
     *
     * @param  integer $id
     * @return bool
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }
}
