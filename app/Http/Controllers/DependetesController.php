<?php

namespace App\Http\Controllers;

use App\Services\ListaNegraService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\DependeteService;
use App\Http\Requests\DependeteCreateRequest;
use App\Http\Requests\DependeteUpdateRequest;
use Illuminate\Support\Facades\Config;

/**
 * @property DependeteService service
 */
class DependetesController extends Controller
{
    /**
     * @var ListaNegraService
     */
    private $listaNegraService;

    /**
     * DependetesController constructor.
     * @param DependeteService $dependete_service
     * @param ListaNegraService $listaNegraService
     */
    public function __construct(DependeteService $dependete_service, ListaNegraService $listaNegraService)
    {
        $this->service = $dependete_service;
        $this->listaNegraService = $listaNegraService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dependetes = $this->service->paginated();
        return view('dependetes.index')->with('dependetes', $dependetes);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $dependetes = $this->service->search($request->search);
        return view('dependetes.index')->with('dependetes', $dependetes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dependetes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DependeteCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DependeteCreateRequest $request)
    {
        if(($this->listaNegraService->isValid($request->cpf))) {
            return redirect(route('dependetes.create'))
                ->withErrors('CPF bloqueado, entre em contato com o <a target="_blank" href="mailto:'.Config::get('mail.from.address').'">adiministrador</a>.')
                ->withInput($request->input());
        }

        if($this->service->paginated()->count() >= auth()->user()->meta->depedente) {
            return redirect(route('dependetes.create'))
                ->withErrors('Limite de dependente excedido, entre em contato com o <a target="_blank" href="mailto:'.Config::get('mail.from.address').'">adiministrador</a>.')
                ->withInput($request->input());
        }

        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('dependetes.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('dependetes.index'))->withErrors('Failed to create');
    }

    /**
     * Display the dependete.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dependete = $this->service->find($id);
        return view('dependetes.show')->with('dependete', $dependete);
    }

    /**
     * Show the form for editing the dependete.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dependete = $this->service->find($id);
        return view('dependetes.edit')->with('dependete', $dependete);
    }

    /**
     * Update the dependetes in storage.
     *
     * @param  DependeteUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DependeteUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the dependetes from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('dependetes.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('dependetes.index'))->withErrors('Failed to delete');
    }
}
