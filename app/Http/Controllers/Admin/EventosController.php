<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\EventoService;
use App\Http\Requests\EventoCreateRequest;
use App\Http\Requests\EventoUpdateRequest;

/**
 * @property EventoService service
 */
class EventosController extends Controller
{
    public function __construct(EventoService $evento_service)
    {
        $this->service = $evento_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventos = $this->service->paginated();
        return view('admin.eventos.index')->with('eventos', $eventos);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $eventos = $this->service->search($request->search);
        return view('admin.eventos.index')->with('eventos', $eventos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.eventos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EventoCreateRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(EventoCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('admin.eventos.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('admin.eventos.index'))->withErrors('Failed to create');
    }

    /**
     * Display the evento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $evento = $this->service->find($id);
        return view('admin.eventos.show')->with('evento', $evento);
    }

    /**
     * Show the form for editing the evento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $evento = $this->service->find($id);
        return view('admin.eventos.edit')->with('evento', $evento);
    }

    /**
     * Update the eventos in storage.
     *
     * @param  EventoUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventoUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Update the eventos in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function copy($id)
    {
        $model = $this->service->find($id);

        $result = $this->service->create($model->only([
            'status',
            'nome',
            'time_1_id',
            'time_2_id',
            'estadio_id',
            'quantidade_alta_valor',
            'quantidade_alta',
            'quantidade_baixa_valor',
            'quantidade_baixa',
            'quantidade_especial',
            'quantidade_especial_valor',
            'data_evento',
            'descricao',
            'foto',
        ]));

        if ($result) {
            return redirect(route('admin.eventos.edit', ['id' => $result->id]))->with('message', 'Successfully copy');
        }

        return redirect(route('admin.eventos.index'))->withErrors('Failed to copy');
    }

    /**
     * Remove the eventos from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('admin.eventos.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('admin.eventos.index'))->withErrors('Failed to delete');
    }
}
