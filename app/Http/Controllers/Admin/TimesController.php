<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\TimeService;
use App\Http\Requests\TimeCreateRequest;
use App\Http\Requests\TimeUpdateRequest;

class TimesController extends Controller
{
    public function __construct(TimeService $time_service)
    {
        $this->service = $time_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $times = $this->service->paginated();
        return view('admin.times.index')->with('times', $times);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $times = $this->service->search($request->search);
        return view('admin.times.index')->with('times', $times);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.times.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TimeCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TimeCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('times.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('times.index'))->withErrors('Failed to create');
    }

    /**
     * Display the time.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $time = $this->service->find($id);
        return view('admin.times.show')->with('time', $time);
    }

    /**
     * Show the form for editing the time.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $time = $this->service->find($id);
        return view('admin.times.edit')->with('time', $time);
    }

    /**
     * Update the times in storage.
     *
     * @param  TimeUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TimeUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the times from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('times.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('times.index'))->withErrors('Failed to delete');
    }
}
