<?php

namespace App\Http\Controllers\Admin;

use App\Services\EventoService;
use App\Services\ListaNegraService;
use App\Services\PessoaService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RegistroEventoService;
use App\Http\Requests\RegistroEventoCreateRequest;
use App\Http\Requests\RegistroEventoUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * @property RegistroEventoService service
 */
class RegistroEventosController extends Controller
{
    /**
     * @var EventoService
     */
    private $eventoService;
    /**
     * @var ListaNegraService
     */
    private $listaNegraService;
    /**
     * @var PessoaService
     */
    private $pessoaService;

    /**
     * RegistroEventosController constructor.
     * @param RegistroEventoService $registro_evento_service
     * @param EventoService $eventoService
     * @param ListaNegraService $listaNegraService
     * @param PessoaService $pessoaService
     */
    public function __construct(RegistroEventoService $registro_evento_service, EventoService $eventoService, ListaNegraService $listaNegraService, PessoaService $pessoaService)
    {
        $this->service = $registro_evento_service;
        $this->eventoService = $eventoService;
        $this->listaNegraService = $listaNegraService;
        $this->pessoaService = $pessoaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registro_eventos = $this->service->paginatedAdmin();
        return view('admin.registro_eventos.index')->with('registro_eventos', $registro_eventos);
    }


    /**
     * Display the registro_evento.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchView(Request $request)
    {
        $registro_eventos = (isset($request->search)) ? $this->service->search($request->search) : new Collection();
        return view('admin.registro_eventos.search')->with('registro_eventos', $registro_eventos);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $registro_eventos = $this->service->search($request->search);
        return view('admin.registro_eventos.index')->with('registro_eventos', $registro_eventos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.registro_eventos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RegistroEventoCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegistroEventoCreateRequest $request)
    {
        DB::beginTransaction();
        if($this->listaNegraService->isValid(auth()->user()->cpf)) {
            DB::rollBack();
            return back()->withErrors('Você não pode assistir jogos no momento')->withInput($request->input());
        }

        $evento = $this->eventoService->find($request->get('evento_id'));
        switch($request->assento) {
            case '0':
                if($evento->quantidade_alta <= 0) {
                    DB::rollBack();
                    return back()->withErrors('Assento alto lotado')->withInput($request->input());
                }
                break;
            case '1':
                if($evento->quantidade_baixa <= 0) {
                    DB::rollBack();
                    return back()->withErrors('Assento baixo lotado')->withInput($request->input());
                }
                break;
            case '2':
                if($evento->quantidade_especial <= 0) {
                    DB::rollBack();
                    return back()->withErrors('Assento especial lotado')->withInput($request->input());
                }
                break;
        }

        if($request->tipo == '0') {
            $result = $this->service->create([
                'user_id' => $request->get('user_id'),
                'evento_id' => $evento->id,
                'uuid' =>  (string) Str::uuid(),
            ]);
        } else {
            $pessoa = $this->pessoaService->model->firstOrCreate([
                'nome' => $request->nome,
                'email' => $request->email,
                'cpf' => $request->cpf
            ]);
            if($this->listaNegraService->isValid($pessoa->cpf)) {
                DB::rollBack();
                return back()->withErrors('O dependente ('.$pessoa->nome.') não pode assistir jogos no momento')->withInput($request->input());
            }
            $result = $this->service->create([
                'evento_id' => $evento->id,
                'pessoa_id' => $pessoa->id,
                'uuid' =>  (string) Str::uuid(),
            ]);
        }
        switch($request->assento) {
            case '0':
                $evento->update([
                    "quantidade_alta" => $evento->quantidade_alta - 1
                ]);
                break;
            case '1':
                $evento->update([
                    "quantidade_baixa" => $evento->quantidade_baixa - 1
                ]);
                break;
            case '2':
                $evento->update([
                    "quantidade_especial" => $evento->quantidade_especial - 1
                ]);
                break;
        }

        if ($result) {
            DB::commit();
            return redirect(route('admin.registro_eventos.index'))->with('message', 'Successfully created');
        }

        DB::rollBack();
        return redirect(route('admin.registro_eventos.index'))->withErrors('Failed to create');
    }

    /**
     * Display the registro_evento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registro_evento = $this->service->find($id);
        return view('admin.registro_eventos.show')->with('registro_evento', $registro_evento);
    }

    /**
     * Show the form for editing the registro_evento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $registro_evento = $this->service->find($id);
        return view('admin.registro_eventos.edit')->with('registro_evento', $registro_evento);
    }

    /**
     * Update the registro_eventos in storage.
     *
     * @param  App\Http\Requests\RegistroEventoUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RegistroEventoUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the registro_eventos from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('admin.registro_eventos.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('admin.registro_eventos.index'))->withErrors('Failed to delete');
    }
}
