<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ListaNegraService;
use App\Http\Requests\ListaNegraCreateRequest;
use App\Http\Requests\ListaNegraUpdateRequest;
use JansenFelipe\FakerBR\FakerBR;

/**
 * @property ListaNegraService service
 */
class ListaNegrasController extends Controller
{
    public function __construct(ListaNegraService $lista_negra_service)
    {
        $this->service = $lista_negra_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lista_negras = $this->service->paginated();
        return view('admin.lista_negras.index')->with('lista_negras', $lista_negras);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $lista_negras = $this->service->search($request->search);
        return view('admin.lista_negras.index')->with('lista_negras', $lista_negras);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lista_negras.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ListaNegraCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ListaNegraCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('lista_negras.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('lista_negras.index'))->withErrors('Failed to create');
    }

    /**
     * Display the lista_negra.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lista_negra = $this->service->find($id);
        return view('admin.lista_negras.show')->with('lista_negra', $lista_negra);
    }

    /**
     * Show the form for editing the lista_negra.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lista_negra = $this->service->find($id);
        return view('admin.lista_negras.edit')->with('lista_negra', $lista_negra);
    }

    /**
     * Update the lista_negras in storage.
     *
     * @param  ListaNegraUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ListaNegraUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the lista_negras from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('lista_negras.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('lista_negras.index'))->withErrors('Failed to delete');
    }

    public function sync() {
        $faker = \Faker\Factory::create();
        $faker->addProvider(new FakerBR($faker));

        if(random_int(0, 1)) {

            for ($i = 0; $i < random_int(2, 10); $i++) {
                $black_list[$i]['cpf'] = $this->mask($faker->cpf, '###.###.###-##');
            }
            $this->service->model->insert($black_list);

            return redirect(route('lista_negras.index'))->with('message', count($black_list) . ' Records found and updated');
        } else {
            return redirect(route('lista_negras.index'))->withErrors('No records found to update');
        }
    }

    private function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++)
        {
            if($mask[$i] == '#')
            {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else
            {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

}
