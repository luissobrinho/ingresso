<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\EstadioService;
use App\Http\Requests\EstadioCreateRequest;
use App\Http\Requests\EstadioUpdateRequest;

/**
 * @property EstadioService service
 */
class EstadiosController extends Controller
{
    public function __construct(EstadioService $estadio_service)
    {
        $this->service = $estadio_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $estadios = $this->service->paginated();
        return view('admin.estadios.index')->with('estadios', $estadios);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $estadios = $this->service->search($request->search);
        return view('admin.estadios.index')->with('estadios', $estadios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.estadios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\EstadioCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EstadioCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('estadios.edit', ['id' => $result->id]))->with('message', 'O estádio foi criado com sucesso');
        }

        return redirect(route('estadios.index'))->withErrors('Failed to create');
    }

    /**
     * Display the estadio.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estadio = $this->service->find($id);
        return view('admin.estadios.show')->with('estadio', $estadio);
    }

    /**
     * Show the form for editing the estadio.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estadio = $this->service->find($id);
        return view('admin.estadios.edit')->with('estadio', $estadio);
    }

    /**
     * Update the estadios in storage.
     *
     * @param  App\Http\Requests\EstadioUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EstadioUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Nome do estádio foi alterado com sucesso');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the estadios from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('estadios.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('estadios.index'))->withErrors('Failed to delete');
    }
}
