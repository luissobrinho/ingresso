<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\EventoService;
use App\Http\Requests\EventoCreateRequest;
use App\Http\Requests\EventoUpdateRequest;

class EventosController extends Controller
{
    public function __construct(EventoService $evento_service)
    {
        $this->service = $evento_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $eventos = $this->service->paginated();
        return view('eventos.index')->with('eventos', $eventos);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $eventos = $this->service->search($request->search);
        return view('eventos.index')->with('eventos', $eventos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\EventoCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventoCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('eventos.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('eventos.index'))->withErrors('Failed to create');
    }

    /**
     * Display the evento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $evento = $this->service->find($id);
        return view('eventos.show')->with('evento', $evento);
    }

    /**
     * Show the form for editing the evento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $evento = $this->service->find($id);
        return view('eventos.edit')->with('evento', $evento);
    }

    /**
     * Update the eventos in storage.
     *
     * @param  App\Http\Requests\EventoUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventoUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the eventos from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('eventos.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('eventos.index'))->withErrors('Failed to delete');
    }
}
