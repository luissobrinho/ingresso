<?php

namespace App\Http\Controllers;

use App\Facades\PagSeguro;
use App\Services\EventoService;
use App\Services\ListaNegraService;
use App\Services\PessoaService;
use Illuminate\Http\Request;
use App\Services\RegistroEventoService;
use App\Http\Requests\RegistroEventoCreateRequest;
use App\Http\Requests\RegistroEventoUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

/**
 * @property RegistroEventoService service
 */
class RegistroEventosController extends Controller
{
    /**
     * @var ListaNegraService
     */
    private $listaNegraService;
    /**
     * @var EventoService
     */
    private $eventoService;
    /**
     * @var PessoaService
     */
    private $pessoaService;

    /**
     * RegistroEventosController constructor.
     * @param RegistroEventoService $registro_evento_service
     * @param ListaNegraService $listaNegraService
     * @param EventoService $eventoService
     * @param PessoaService $pessoaService
     */
    public function __construct(RegistroEventoService $registro_evento_service, ListaNegraService $listaNegraService, EventoService $eventoService, PessoaService $pessoaService)
    {
        $this->service = $registro_evento_service;
        $this->listaNegraService = $listaNegraService;
        $this->eventoService = $eventoService;
        $this->pessoaService = $pessoaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(request()->has('transacao_id')) {
            $this->service->model->where('user_id', auth()->user()->id)->update(['status' => 1]);
        }
        $registro_eventos = $this->service->paginated();
        return view('registro_eventos.index')->with('registro_eventos', $registro_eventos);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $registro_eventos = $this->service->search($request->search);
        return view('registro_eventos.index')->with('registro_eventos', $registro_eventos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registro_eventos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RegistroEventoCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegistroEventoCreateRequest $request)
    {
        return DB::transaction(function () use ($request) {
            $quantidade = 0;
            $valor = 0;

            if($this->listaNegraService->isValid(auth()->user()->cpf)) {
                DB::rollBack();
                return back()->withErrors('Você não pode assistir jogos no momento')->withInput($request->input());
            }

            $evento = $this->eventoService->find($request->get('evento_id'));
            if($request->pessoa_id) {
                foreach ($request->pessoa_id as $dependentes) {
                    switch ($request->quantidade[$dependentes]) {
                        case 1:
                            $quantidade++;
                            $dep = auth()->user()->dependentes()->select("nome", "email", "cpf")->where('id', $dependentes)->first();
                            $this->pessoaService->model->firstOrCreate($dep->toArray());
                            if ($this->listaNegraService->isValid($dep->cpf)) {
                                DB::rollBack();
                                return back()->withErrors('O dependente (' . $dep->nome . ') não pode assistir jogos no momento')->withInput($request->input());
                            }
                            $this->service->create([
                                'user_id' => $request->get('user_id'),
                                'evento_id' => $evento->id,
                                'pessoa_id' => $dependentes,
                                'uuid' => (string)Str::uuid(),
                                'meia' => $request->has("meia[$dependentes]"),
                                'assento' => 1,
                                'valor' => $request->has("meia[$dependentes]") ? $evento->quantidade_alta_valor / 2 : $evento->quantidade_alta_valor
                            ]);
                            if ($evento->quantidade_alta <= 0) {
                                DB::rollBack();
                                return back()->withErrors('Assento alto lotado')->withInput($request->input());
                            }
                            $evento->update([
                                "quantidade_alta" => $evento->quantidade_alta - 1
                            ]);
                            $valor += $request->has("meia[$dependentes]") ? $evento->quantidade_alta_valor / 2 : $evento->quantidade_alta_valor;
                            break;
                        case 2:
                            $quantidade++;
                            $dep = auth()->user()->dependentes()->select("nome", "email", "cpf")->where('id', $dependentes)->first();
                            $this->pessoaService->model->firstOrCreate($dep->toArray());
                            if ($this->listaNegraService->isValid($dep->cpf)) {
                                DB::rollBack();
                                return back()->withErrors('O dependente (' . $dep->nome . ') não pode assistir jogos no momento')->withInput($request->input());
                            }
                            $this->service->create([
                                'user_id' => $request->get('user_id'),
                                'evento_id' => $evento->id,
                                'pessoa_id' => $dependentes,
                                'uuid' => (string)Str::uuid(),
                                'meia' => $request->has("meia[$dependentes]"),
                                'assento' => 2,
                                'valor' => $request->has("meia[$dependentes]") ? $evento->quantidade_baixa_valor / 2 : $evento->quantidade_baixa_valor
                            ]);
                            if ($evento->quantidade_baixa <= 0) {
                                DB::rollBack();
                                return back()->withErrors('Assento baixo lotado')->withInput($request->input());
                            }
                            $evento->update([
                                "quantidade_baixa" => $evento->quantidade_baixa - 1
                            ]);
                            $valor += $request->has("meia[$dependentes]") ? $evento->quantidade_baixa_valor / 2 : $evento->quantidade_baixa_valor;
                            break;
                        case 3:
                            $quantidade++;
                            $dep = auth()->user()->dependentes()->select("nome", "email", "cpf")->where('id', $dependentes)->first();
                            $this->pessoaService->model->firstOrCreate($dep->toArray());
                            if ($this->listaNegraService->isValid($dep->cpf)) {
                                DB::rollBack();
                                return back()->withErrors('O dependente (' . $dep->nome . ') não pode assistir jogos no momento')->withInput($request->input());
                            }
                            $this->service->create([
                                'user_id' => $request->get('user_id'),
                                'evento_id' => $evento->id,
                                'pessoa_id' => $dependentes,
                                'uuid' => (string)Str::uuid(),
                                'meia' => $request->has("meia[$dependentes]"),
                                'assento' => 3,
                                'valor' => $request->has("meia[$dependentes]") ? $evento->quantidade_especial_valor / 2 : $evento->quantidade_especial_valor
                            ]);
                            if ($evento->quantidade_especial <= 0) {
                                DB::rollBack();
                                return back()->withErrors('Assento especial lotado')->withInput($request->input());
                            }
                            $evento->update([
                                "quantidade_especial" => $evento->quantidade_especial - 1
                            ]);
                            $valor += $request->has("meia[$dependentes]") ? $evento->quantidade_especial_valor / 2 : $evento->quantidade_especial_valor;
                            break;
                    }
                }
            }

            $result = $this->service->create([
                'evento_id' => $evento->id,
                'user_id' => $request->get('user_id'),
                'uuid' =>  (string) Str::uuid(),
                'meia' => $request->has('meia')
            ]);

            switch($request->quantidade_eu) {
                case 1:
                    if ($evento->quantidade_alta <= 0) {
                        DB::rollBack();
                        return back()->withErrors('Assento alta lotado')->withInput($request->input());
                    }
                    $evento->update([
                        "quantidade_alta" => $evento->quantidade_alta - 1
                    ]);
                    $valor += ($request->has('meia')) ? $evento->quantidade_alta_valor / 2 : $evento->quantidade_alta_valor;
                    $result->update([
                        'valor' =>  ($request->has('meia')) ? $evento->quantidade_alta_valor / 2 : $evento->quantidade_alta_valor,
                        'assento' => 1
                    ]);
                    break;
                case 2:

                    if ($evento->quantidade_baixa <= 0) {
                        DB::rollBack();
                        return back()->withErrors('Assento baixo lotado')->withInput($request->input());
                    }
                    $evento->update([
                        "quantidade_baixa" => $evento->quantidade_baixa - 1
                    ]);
                    $valor += ($request->has('meia')) ? $evento->quantidade_baixa_valor / 2 : $evento->quantidade_baixa_valor;
                    $result->update([
                        'valor' => ($request->has('meia')) ? $evento->quantidade_baixa_valor / 2 : $evento->quantidade_baixa_valor,
                        'assento' => 2
                    ]);
                    break;
                case 3:

                    if ($evento->quantidade_especial <= 0) {
                        DB::rollBack();
                        return back()->withErrors('Assento especial lotado')->withInput($request->input());
                    }
                    $evento->update([
                        "quantidade_especial" => $evento->quantidade_especial - 1
                    ]);
                    $valor += ($request->has('meia')) ? $evento->quantidade_especial_valor / 2 : $evento->quantidade_especial_valor;
                    $result->update([
                        'valor' => ($request->has('meia')) ? $evento->quantidade_especial_valor / 2 : $evento->quantidade_especial_valor,
                        'assento' => 3
                    ]);
                    break;
            }
            if ($result) {
                try {
                    $data = [
                        'items' => [
                            [
                                'id' => $evento->id,
                                'description' => 'Ingresso do ' . $evento->nome . ' - ' . $evento->time_a->nome . ' X ' . $evento->time_b->nome,
                                'quantity' => 1,
                                'amount' => $valor,
                                'weight' => '0',
                                'shippingCost' => '0',
                                'width' => '0',
                                'height' => '0',
                                'length' => '0',
                            ],
                        ],
                        'shipping' => [
                            'address' => [
                                'postalCode' => '06410030',
                                'street' => 'Rua Leonardo Arruda',
                                'number' => '12',
                                'district' => 'Jardim dos Camargos',
                                'city' => 'Barueri',
                                'state' => 'SP',
                                'country' => 'BRA',
                            ],
                            'type' => 2,
                            'cost' => 0.0,
                        ],
                        'sender' => [
                            'email' => auth()->user()->email,
                            'name' => auth()->user()->name,
                            'documents' => [
                                [
                                    'number' => str_replace(['.', '-'], '', auth()->user()->cpf),
                                    'type' => 'CPF'
                                ]
                            ],
                            'phone' => auth()->user()->meta->phone,
                            'bornDate' => auth()->user()->meta->bornDate->format('Y-m-d'),
                        ],
                        'redirectURL' => route('pagseguro.redirect'),
                        'notificationURL' => route('pagseguro.notification')
                    ];

                    $checkout = PagSeguro::checkout()->createFromArray($data);
                    $credentials = PagSeguro::credentials()->get();
                    $information = $checkout->send($credentials); // Retorna um objeto de laravel\pagseguro\Checkout\Information\Information
                    if ($information) {
                        DB::commit();
                        $result->update([
                            'link_pagamento' => $information->getLink(),
                            'code_pagamento' => $information->getCode(),
                            'data_pagamento' => $information->getDate(),
                        ]);

                        return Redirect::intended($information->getLink());
                    }
                } catch (\Exception $exception) {
                    DB::rollBack();
                    return back()->withErrors($exception->getMessage());
                }
                DB::commit();
                return back()->with('message', 'Successfully created');
            }
            DB::rollBack();
            return back()->withErrors('Failed to create');
        });
    }

    /**
     * Display the registro_evento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registro_evento = $this->service->find($id);
        return view('registro_eventos.show')->with('registro_evento', $registro_evento);
    }

    /**
     * Show the form for editing the registro_evento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $registro_evento = $this->service->find($id);
        return view('registro_eventos.edit')->with('registro_evento', $registro_evento);
    }

    /**
     * Update the registro_eventos in storage.
     *
     * @param  RegistroEventoUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RegistroEventoUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the registro_eventos from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('registro_eventos.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('registro_eventos.index'))->withErrors('Failed to delete');
    }
}
