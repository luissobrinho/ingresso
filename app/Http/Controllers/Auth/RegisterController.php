<?php

namespace App\Http\Controllers\Auth;

use DB;
use Illuminate\Http\UploadedFile;
use Validator;
use App\Services\UserService;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

/**
 * @property UserService service
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('guest');
        $this->service = $userService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        /** @var UploadedFile $arquivo_photo */
        $arquivo_photo = $data['arquivo_photo'];
        $arquivo_photo_name = date('YmdHis')."-photo-$data[cpf]".'.'.$arquivo_photo->getClientOriginalExtension();
        if($arquivo_photo->move('img/fotos', $arquivo_photo_name)) {
            $data['arquivo_photo'] = 'img/fotos/'.$arquivo_photo_name;
        } else {
            $data['arquivo_photo'] = '';
        }

        /** @var UploadedFile $arquivo_identidade */
        $arquivo_identidade = $data['arquivo_identidade'];
        $arquivo_identidade_name = date('YmdHis')."-identidade-$data[cpf]".'.'.$arquivo_identidade->getClientOriginalExtension();
        if($arquivo_identidade->move('img/fotos', $arquivo_identidade_name)) {
            $data['arquivo_identidade'] = 'img/fotos/'.$arquivo_identidade_name;
        } else {
            $data['arquivo_identidade'] = '';
        }

        /** @var UploadedFile $arquivo_comprovante */
        $arquivo_comprovante = $data['arquivo_conprovante'];
        $arquivo_comprovante_name = date('YmdHis')."-comprovante-$data[cpf]".'.'.$arquivo_comprovante->getClientOriginalExtension();
        if($arquivo_comprovante->move('img/fotos', $arquivo_comprovante_name)) {
            $data['arquivo_comprovante'] = 'img/fotos/'.$arquivo_comprovante_name;
        } else {
            $data['arquivo_comprovante'] = '';
        }

        return DB::transaction(function() use ($data) {
            $user = User::create([
                'email' => $data['email'],
                'name' => $data['name'],
                'cpf' => $data['cpf'],
                'password' => bcrypt($data['password'])
            ]);

            $user = $this->service->create($user, $data['password'], $data);


            $user->meta->update([
                'photo' => $data['arquivo_photo'],
                'identidade' => $data['arquivo_identidade'],
                'comprovante' => $data['arquivo_comprovante'],
            ]);
            return $user;
        });
    }
}
