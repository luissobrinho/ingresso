<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dependete extends Model
{
    use SoftDeletes;

    public $table = "dependetes";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'nome',
		'email',
		'data_nascimento',
		'cpf',
		'user_id',

    ];

    public static $rules = [
        // create rules
        'nome' => 'required',
        'email' => 'required',
        'cpf' => 'required|cpf',
        'data_nascimento' => 'required|date_format:Y-m-d',
        'user_id' => 'required',
    ];


    public function getDependentes($id) {
        return $this->where('user_id', $id)->get();
    }

    // Dependete 
}
