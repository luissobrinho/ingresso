<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistroEvento extends Model
{
    use SoftDeletes;

    public $table = "registro_eventos";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'user_id',
		'evento_id',
		'pessoa_id',
		'uuid',
        'assento',
		'status',
        'valor',
        'meia',
        'link_pagamento',
        'code_pagamento',
        'data_pagamento',
    ];

    public static $rules = [
        // create rules
        'quantidade_eu' => 'numeric|min:1',
//        'user_id' => 'unique:registro_eventos,user_id,NULL,pessoa_id'
    ];

    // RegistroEvento

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function pessoa() {
        return $this->belongsTo(Pessoa::class);
    }

    public function evento() {
        return $this->belongsTo(Evento::class);
    }
}
