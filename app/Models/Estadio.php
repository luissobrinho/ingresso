<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estadio extends Model
{
    use SoftDeletes;

    public $table = "estadios";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'nome',

    ];

    public static $rules = [
        // create rules
        'nome' => 'required'
    ];

    public function getEstadios() {
        $aaray = ['nome' => '<< Selecione >>', 'id' => null];
        return $this->get()->add((new Estadio($aaray)))->sortBy('id');
    }

    // Estadio 
}
