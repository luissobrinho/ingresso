<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Time extends Model
{
    use SoftDeletes;

    public $table = "times";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'nome',

    ];

    public static $rules = [
        // create rules
        'nome' => 'required'
    ];

    // Time

    public function getTimes() {
        $aaray = ['nome' => '<< Selecione >>', 'id' => null];
        return $this->get()->add((new Time($aaray)))->sortBy('id');
    }
}
