<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListaNegra extends Model
{
    use SoftDeletes;

    public $table = "lista_negras";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'cpf',
		'user_id',

    ];

    public static $rules = [
        // create rules
        'cpf' => 'nullable|cpf'
    ];

    // ListaNegra

    public function user() {
        return $this->belongsTo(User::class);
    }
}
