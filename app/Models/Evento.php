<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evento extends Model
{
    use SoftDeletes;

    public $table = "eventos";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        'id',
        'status',
        'nome',
        'time_1_id',
        'time_2_id',
        'estadio_id',
        'quantidade_alta_valor',
        'quantidade_alta',
        'quantidade_baixa_valor',
        'quantidade_baixa',
        'quantidade_especial',
        'quantidade_especial_valor',
        'data_evento',
        'descricao',
        'foto',

    ];

    public function setQuantidadeAltaValorAttribute($value)
    {
        $this->attributes['quantidade_alta_valor'] = number_format(str_replace(',', '.', $value), 2);

    }

    public function setQuantidadeBaixoValorAttribute($value)
    {
        $this->attributes['quantidade_baixa_valor'] = number_format(str_replace(',', '.', $value), 2);

    }

    public function setQuantidadeEspecialValorAttribute($value)
    {
        $this->attributes['quantidade_especial_valor'] = number_format(str_replace(',', '.', $value), 2);

    }

    public $dates = [
        'data_evento',
    ];

    public function time_a() {
        return $this->belongsTo(Time::class, 'time_1_id');
    }

    public function time_b() {
        return $this->belongsTo(Time::class, 'time_2_id');
    }

    public function estadio() {
        return $this->belongsTo(Estadio::class);
    }

    public static $rules = [
        // create rules
        'nome' => 'required',
        'time_1_id' => 'required',
        'time_2_id' => 'required',
        'estadio_id' => 'required',
        'quantidade_alta_valor' => 'required',
        'quantidade_alta' => 'required',
        'quantidade_baixa' => 'required',
        'quantidade_baixa_valor' => 'required',
        'quantidade_especial' => 'required',
        'quantidade_especial_valor' => 'required',
        'data_evento' => 'required',
        'descricao' => 'required',
        'arquivo' => 'required',
    ];

    // Evento
    public function EventValid() {
        $aaray = ['nome' => '<< Selecione >>', 'id' => null];
        return $this->orWhere('quantidade_alta', '>', 0)
            ->orWhere('quantidade_baixa', '>', 0)
            ->orWhere('quantidade_especial', '>', 0)
            ->get()
            ->add((new Evento($aaray)))->sortBy('id');
    }
}
