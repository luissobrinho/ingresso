<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pessoa extends Model
{
    use SoftDeletes;

    public $table = "pessoas";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'nome',
		'email',
		'cpf',

    ];

    public static $rules = [
        // create rules
    ];

    // Pessoa 
}
