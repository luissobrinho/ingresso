<?php
/**
 * Created by PhpStorm.
 * User: luiseduardo
 * Date: 2019-01-21
 * Time: 23:19
 */

namespace App\Facades;


use laravel\pagseguro\Facades\Checkout;
use App\Facades\Credentials;
use laravel\pagseguro\Facades\Item;
use laravel\pagseguro\Facades\Transaction;

class PagSeguro
{
    /**
     * @return Checkout
     */
    public static function checkout()
    {
        return new Checkout();
    }

    /**
     * @return Credentials
     */
    public static function credentials()
    {
        return new Credentials();
    }

    /**
     * @return Item
     */
    public static function item()
    {
        return new Item();
    }

    /**
     * @return Transaction
     */
    public static function transaction()
    {
        return new Transaction();
    }
}