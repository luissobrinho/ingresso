<?php
/**
 * Created by PhpStorm.
 * User: luiseduardo
 * Date: 2019-01-22
 * Time: 23:20
 */

namespace App\Facades;


use Illuminate\Support\Facades\Config;
use laravel\pagseguro\Credentials\Credentials as PagSeguroCredentials;


class Credentials
{

    /**
     * Get Default Credentials
     * @param string $token
     * @param string $email
     * @return Credentials
     */
    public function create($token, $email)
    {
        return new PagSeguroCredentials($token, $email);
    }

    /**
     * Get Default Credentials
     * @return Credentials
     */
    public function get()
    {
        $data = Config::get('laravelpagseguro.credentials');
        return $this->create($data['token'], $data['email']);
    }
}