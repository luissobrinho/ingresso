@extends('layouts.app')

@section('pageTitle') Registro de Evento &raquo; Criar @stop

@section('content')
    <div class="col-md-6 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'registro_eventos.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Registro de Evento: Criar</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">

                    {!! Form::open(['route' => 'registro_eventos.store']) !!}

                    @form_maker_table("registro_eventos")

                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary pull-right']) !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop