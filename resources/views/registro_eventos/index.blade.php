@extends('layouts.app')

@section('pageTitle') Registro de Evento &raquo; Índice @stop

@section('content')
    <div class="col-md-8 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'registro_eventos.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Registro de Evento</h1>
                </div>
            </div>

            <div class="card-body">
                <div class="col-md-12">
                    @if ($registro_eventos->isEmpty())
                        <div class="well text-center">Nenhum registro do evento encontrado.</div>
                    @else
                        <table class="table table-striped">
                            <thead>
                            <th>Nome</th>
                            <th>Evento</th>
                            <th>Status</th>
                            <th class="text-right" width="200px">Ação</th>
                            </thead>
                            <tbody>
                            @foreach($registro_eventos as $registro_evento)
                                <tr>
                                    <td>
                                        @if($registro_evento->pessoa)
                                            {{ $registro_evento->pessoa->nome }}
                                        @else
                                            {{ $registro_evento->user->name }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $registro_evento->evento->nome }} - {{ $registro_evento->evento->time_a->nome }} X {{ $registro_evento->evento->time_b->nome }}
                                    </td>
                                    <td>
                                        @if($registro_evento->status)
                                            Pagamento aprovado
                                        @else
                                            Aguardando confirmação de pagamento
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        @if($registro_evento->status)
                                            <a class="btn btn-default btn-xs pull-right raw-margin-right-16" href="{!! route('registro_eventos.show', [$registro_evento->id]) !!}"><i class="fa fa-eye"></i> Ver</a>
                                        @else
                                            <a href="{{ $registro_evento->link_pagamento }}" class="btn btn-success btn-xs pull-right raw-margin-right-16" target="_blank"><i class="fa fa-money"></i> Pagar</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-12 text-center float-right">
                                {!! $registro_eventos; !!}
                            </div>
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </div>


@stop