@extends('layouts.app')
@section('pageTitle') RegistroEventos &raquo; Show @stop
@section('content')
    <style>
        svg {
            border: 1px solid;
        }
    </style>
    <div class="container">
        <div class="col-md-12">

            <!-- Put fields here -->
            <div class="card bg-success">
                <div class="card-body text-white">
                    <div class="row">
                        <div class="col-md-6" style="border-right: 1px solid #fff;">
                            <div class="float-left">
                                <h2>{{ $registro_evento->evento->nome }} - {{ $registro_evento->evento->time_a->nome }}
                                    X {{ $registro_evento->evento->time_b->nome }}</h2>
                                <p class="card-text">
                                    Descrição: <strong>{{ $registro_evento->evento->descricao }}</strong> <br>
                                    Valor  @if($registro_evento->meia) (Meia) @endif: <strong>R${!! number_format($registro_evento->valor, 2, ',' , '.') !!}</strong><br>
                                    Estádio: <strong>{{ $registro_evento->evento->estadio->nome }}</strong>
                                </p>
                                <p class="card-text">Data do Jogo: <span style="font-weight: 900">{{ $registro_evento->evento->data_evento->format('d/m/Y') }} </span><br>
                                    Data da Compra: <span style="font-weight: 900">{{ $registro_evento->created_at->format('d/m/Y') }}</span>
                                </p>
                                @if($registro_evento->user)
                                    <p>Comprador: <strong>{!! $registro_evento->user->name !!} - {!! $registro_evento->user->cpf !!}</strong> <br>
                                        @if($registro_evento->pessoa)
                                            Presentiado: <strong>{!! $registro_evento->pessoa->nome !!} - {!! $registro_evento->pessoa->cpf !!}</strong>
                                        @endif
                                        @if($registro_evento->assento == 1)
                                            <br> Assento: <strong>{{ 'Alto' }}</strong>
                                        @elseif($registro_evento->assento == 2)
                                            <br> Assento: <strong>{{ 'Baixa' }}</strong>
                                        @else
                                            <br> Assento: <strong>{{ 'Especial' }}</strong>
                                        @endif
                                    </p>
                                @else
                                    <p>Comprador: <strong>{!! $registro_evento->pessoa->nome !!} - {!! $registro_evento->pessoa->cpf !!}</strong> <br>
                                        @if($registro_evento->assento == 1)
                                            <br> Assento: <strong>{{ 'Alto' }}</strong>
                                        @elseif($registro_evento->assento == 2)
                                            <br> Assento: <strong>{{ 'Baixa' }}</strong>
                                        @else
                                            <br> Assento: <strong>{{ 'Especial' }}</strong>
                                        @endif
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="float-left col-6">
                                <img src="{{ asset($registro_evento->user->meta->photo) }}" alt="" class="img-fluid img-thumbnail">
                            </div>
                            <div class="float-right text-right col-6">
                                {!! (new \LaravelQRCode\QRCodeFactory)->text($registro_evento->uuid)->setSize(6)->svg() !!}
                                <br>
                                <p style="font-size: 70%">{!! $registro_evento->uuid !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop