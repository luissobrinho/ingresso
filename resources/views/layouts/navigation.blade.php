@auth()
    @if (Gate::allows('admin'))
        <li class="nav-item @if(Request::is('admin/dashboard', 'admin/dashboard/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/dashboard') !!}"> Dashboard</a>
        </li>
        <li class="nav-item @if(Request::is('admin/users', 'admin/users/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/users') !!}"> Users</a>
        </li>
        <li class="nav-item @if(Request::is('admin/roles', 'admin/roles/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/roles') !!}"> Roles</a>
        </li>
        <li class="nav-item @if(Request::is('admin/times', 'admin/times/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/times') !!}"> Clube</a>
        </li>
        <li class="nav-item @if(Request::is('admin/estadios', 'admin/estadios/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/estadios') !!}"> Estádio</a>
        </li>
        <li class="nav-item @if(Request::is('admin/eventos', 'admin/eventos/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/eventos') !!}"> Eventos</a>
        </li>
        <li class="nav-item @if(Request::is('admin/lista_negras', 'admin/lista_negras/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/lista_negras') !!}"> Lista Negra</a>
        </li>
        <li class="nav-item @if(Request::is('admin/registro_eventos', 'admin/registro_eventos/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/registro_eventos') !!}"> Registros</a>
        </li>
        <li class="nav-item @if(Request::is('admin/registro_eventos/valid/ingress/valid-ingress', 'admin/registro_eventos/*')) active @endif">
            <a class="nav-link" href="{!! url('admin/registro_eventos/valid/ingress/valid-ingress') !!}"> Validar</a>
        </li>
    @else
        <li class="nav-item @if(Request::is('registro_eventos', 'registro_eventos/*')) active @endif">
            <a class="nav-link" href="{!! url('registro_eventos') !!}"> Registros</a>
        </li>
        <li class="nav-item @if(Request::is('dependetes', 'dependetes/*')) active @endif">
            <a class="nav-link" href="{!! url('dependetes') !!}"> Dependetes</a>
        </li>
    @endif
@endguest