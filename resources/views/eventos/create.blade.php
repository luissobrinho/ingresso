@extends('layouts.app')

@section('pageTitle') Eventos &raquo; Criar @stop

@section('content')
    <div class="col-md-6 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="float-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'admin.eventos.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Eventos: Criar</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">

                    {!! Form::open(['route' => 'admin.eventos.store', 'files' => true]) !!}

                    <div class="form-row raw-margin-bottom-15">
                        @form_maker_array([
                        'nome' => ['type' => 'string'],
                        'time_1_id' => ['type' => 'relationship', 'alt_name' => 'Time A', 'model' => 'App\Models\Time', 'label' => 'nome'],
                        'time_2_id' => ['type' => 'relationship', 'alt_name' => 'Time B', 'model' => 'App\Models\Time', 'label' => 'nome'],
                        ])
                    </div>

                    <div class="form-row raw-margin-bottom-15">
                        @form_maker_array([
                        'quantidade_alta' => ['type' => 'number', 'alt_name' => 'Quantidade Alta'],
                        'quantidade_baixa' => ['type' => 'number', 'alt_name' => 'Quantidade Baixa'],
                        'quantidade_especial' => ['type' => 'number', 'alt_name' => 'Quantidade Especial'],
                        'data_evento' => ['type' => 'date', 'alt_name' => 'Data do Evento'],
                        ])
                    </div>

                    <div class="form-row raw-margin-bottom-15">
                        @form_maker_array([
                        'quantidade_alta_valor' => ['type' => 'number', 'alt_name' => 'Valor Quantidade Alta', 'custom' => 'data-mask="###.##0,00"'],
                        'quantidade_baixa_valor' => ['type' => 'number', 'alt_name' => 'Valor Quantidade Baixa', 'custom' => 'data-mask="###.##0,00"'],
                        'quantidade_especial_valor' => ['type' => 'number', 'alt_name' => 'Valor Quantidade Especial', 'custom' => 'data-mask="###.##0,00"'],
                        'data_evento' => ['type' => 'date', 'alt_name' => 'Data do Evento'],
                        ])
                    </div>

                    <div class="form-row raw-margin-bottom-15">
                        @form_maker_array([
                        'valor' => ['type' => 'number', 'alt_name' => 'Valor'],
                        'descricao' => ['type' => 'text', 'alt_name' => 'Descriçao'],
                        'arquivo' => ['type' => 'file', 'alt_name' => 'Foto'],
                        ])
                    </div>

                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary pull-right']) !!}
                    <a href="{{ url('admin/eventos') }}" class="btn btn-secondary"> Cancelar</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop