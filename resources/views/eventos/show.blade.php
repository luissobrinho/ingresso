@extends('layouts.master')

@section('pageTitle') Eventos &raquo; Show @stop

@section('content')

    <div class="col-lg-12 raw-margin-top-85">

        <div class="card mt-4">
            <img class="card-img-top img-fluid" src="{{ url($evento->foto) }}" alt="">
            <div class="card-body">
                <p class="card-text float-right">Data do Jogo: <span style="font-weight: 900">{{ $evento->data_evento->format('d/m/Y') }}</span></p>
                <h3 class="card-title">{{ $evento->nome }}</h3>
                <h4><strong>Alta: </strong>R${{ str_replace('.', ',', $evento->quantidade_alta_valor) }}</h4>
                <h4><strong>Baixa: </strong>R${{ str_replace('.', ',', $evento->quantidade_baixa_valor) }}</h4>
                <h4><strong>Especial: </strong>R${{ str_replace('.', ',', $evento->quantidade_especial_valor) }}</h4>
                <p class="card-text">{{ $evento->descricao }}</p>
            </div>
        </div>
        <!-- /.card -->

        <div class="card card-outline-secondary my-4">
            <div class="card-header">
                Compra de ingresso
            </div>
            <div class="card-body">
                @guest()
                    <h3>Para efetuar a compra é necessário fazer login</h3>
                    <a href="{{ url('/login') }}" class="btn btn-primary">Login</a>
                @else
                    {!! Form::open(['route' => 'registro_eventos.store']) !!}
                    @form_maker_array([
                    'evento_id' => ['type' => 'hidden', 'default_value' => $evento->id],
                    ])
                    <fieldset style="border: 1px solid #f4f4f4; padding: 10px 15px; margin-bottom: 20px">
                        <div class="form-row raw-margin-bottom-15">
                            @form_maker_array([
                            'user_id' => ['type' => 'select', 'alt_name' => 'Eu', 'options' => [auth()->user()->name => auth()->user()->id], 'custom' => 'readonly="readonly"w'],
                            ])
                        </div>
                        <div class="form-row raw-margin-bottom-15">
                            @form_maker_array([
                            'quantidade_eu' => ['type' => 'select', 'alt_name' => 'Assento', 'options' => [
                            '<< Selecione >>' => 0,
                            'Quantidade Alta ('.$evento->quantidade_alta.' Unidades)' => 1,
                            'Quantidade Baixa ('.$evento->quantidade_baixa.' Unidades)' => 2,
                            'Quantidade Especial ('.$evento->quantidade_especial.' Unidades)' => 3
                            ]
                            ],
                            'meia' => ['type' => 'checkbox', 'alt_name' => 'Meia?', 'class' => 'col-lg-2']
                            ])
                        </div>
                    </fieldset>
                    @foreach(app(App\Models\Dependete::class)->getDependentes(auth()->user()->id) as $dependente)
                        <fieldset style="border: 1px solid #f4f4f4; padding: 10px 15px; margin-bottom: 20px">
                            <div class="form-row raw-margin-bottom-15">
                                @form_maker_array([
                                'pessoa_id['.$dependente->id.']' => ['type' => 'select', 'alt_name' => 'Dependente', 'options' => [$dependente->nome => $dependente->id], 'custom' => 'readonly="readonly"'],
                                ])
                            </div>
                            <div class="form-row raw-margin-bottom-15">
                                @form_maker_array([
                                'quantidade['.$dependente->id.']' => ['type' => 'select', 'alt_name' => 'Assento', 'options' => [
                                '<< Selecione >>' => 0,
                                'Quantidade Alta ('.$evento->quantidade_alta.' Unidades)' => 1,
                                'Quantidade Baixa ('.$evento->quantidade_baixa.' Unidades)' => 2,
                                'Quantidade Especial ('.$evento->quantidade_especial.' Unidades)' => 3
                                ]
                                ],
                                'meia['.$dependente->id.']' => ['type' => 'checkbox', 'alt_name' => 'Meia?', 'class' => 'col-lg-2']
                                ])
                            </div>
                        </fieldset>
                    @endforeach

                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary pull-right']) !!}

                    {!! Form::close() !!}
                @endguest
            </div>
        </div>
        <!-- /.card -->

    </div>
@stop