@extends('layouts.master')

@section('pageTitle') Home @stop

@section('content')
    <!-- Jumbotron Header -->
    <header class="jumbotron raw-margin-top-85">
        <h1 class="display-3">{{ env('APP_NAME', 'Laravel') }}</h1>
        <p class="lead">Oferecemos ferramentas seguras e eficazes, além de soluções inteligentes que revolucionaram a forma como produtores criam e administram seus eventos.</p>
    </header>
    <div class="container">
        <div class="row">
            @foreach(app(App\Models\Evento::class)->paginate(25) as $evento)
                <div class="col-lg-3 col-md-6 raw-margin-bottom-15">
                    <div class="card">
                        <img class="card-img-top" src="{{ url($evento->foto) }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{ $evento->nome }}</h5>
                            <p class="card-text">{{ $evento->descricao }}</p>
                            <p class="card-text">Data do Jogo: <span
                                        style="font-weight: 900">{{ $evento->data_evento->format('d/m/Y') }}</span></p>
                        </div>
                        <div class="card-footer">
                            @if($evento->quantidade_alta <= 0 and $evento->quantidade_baixa <= 0 and $evento->quantidade_especial <= 0)
                                <button type="button" class="btn btn-primary disabled">Ver Mais</button>
                                <p class="badge badge-secondary">Esgotado</p>
                            @else
                                <a href="{{ route('eventos.show', ['id' => $evento->id]) }}" class="btn btn-primary">Ver
                                    Mais</a>
                            @endif
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
    <div class="raw-margin-top-15">
        {{ app(App\Models\Evento::class)->paginate(25)->links() }}
    </div>
@endsection