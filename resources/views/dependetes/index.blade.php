@extends('layouts.app')

@section('pageTitle') Dependente &raquo; Índice @stop

@section('content')
<div class="col-md-8 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'dependetes.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Dependente</h1>
                    <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('dependetes.create') !!}">Adicionar novo</a>
                </div>
            </div>

                <div class="card-body">
                    <div class="col-md-12">
                        @if ($dependetes->isEmpty())
                            <div class="well text-center">Nenhum dependete encontrado.</div>
                        @else
                            <table class="table table-striped">
                                <thead>
                                    <th>Nome</th>
                                    <th class="text-right" width="200px">Ação</th>
                                </thead>
                                <tbody>
                                    @foreach($dependetes as $dependete)
                                        <tr>
                                            <td>
                                                <a href="{!! route('dependetes.edit', [$dependete->id]) !!}">{{ $dependete->nome }}</a>
                                            </td>
                                            <td class="text-right">
                                                <form method="post" action="{!! route('dependetes.destroy', [$dependete->id]) !!}">
                                                    {!! csrf_field() !!}
                                                    {!! method_field('DELETE') !!}
                                                    <button class="btn btn-danger btn-sm float-right" type="submit" onclick="return confirm('Tem certeza de que deseja excluir este dependente?')"><i class="fa fa-trash"></i> Excluir</button>
                                                </form>
                                                <a class="btn btn-default btn-sm float-right raw-margin-right-16" href="{!! route('dependetes.edit', [$dependete->id]) !!}"><i class="fa fa-pencil"></i> Editar</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
           </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {!! $dependetes; !!}
        </div>
    </div>

@stop