@extends('layouts.app')

@section('pageTitle') Dependetes &raquo; Editar @stop

@section('content')
<div class="col-md-6 m-auto">
    <div class="card">
        <div class="card-header">
            <div class="col-md-12">
                <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                    {!! Form::open(['route' => 'dependetes.search']) !!}
                    <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                    {!! Form::close() !!}
                </div>
                <h1 class="pull-left">Dependetes: Editar</h1>
                <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('dependetes.create') !!}">Adicionar novo</a>
            </div>
        </div>

        <div class="card-body">
            <div class="col-md-12">
                {!! Form::model($dependete, ['route' => ['dependetes.update', $dependete->id], 'method' => 'patch']) !!}

                <div class="form-row raw-margin-bottom-15">
                    @form_maker_array($dependete, [
                    'nome' => ['type' => 'string', 'alt_name' => 'Nome'],
                    'email' => ['type' => 'string', 'alt_name' => 'E-mail'],
                    ])
                </div>
                <div class="form-row raw-margin-bottom-15">
                    @form_maker_array($dependete, [
                    'cpf' => ['type' => 'string', 'alt_name' => 'CPF'],
                    'data_nascimento' => ['type' => 'date', 'alt_name' => 'Data de Nascimento'],
                    ])
                </div>

                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary pull-right']) !!}
                <a href="{{ url('dependetes') }}" class="btn btn-secondary"> Cancelar</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop
