@extends('layouts.app')

@section('pageTitle') Dependentes &raquo; Criar @stop

@section('content')
    <div class="col-md-6 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'dependetes.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Dependentes: Criar</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">

                    {!! Form::open(['route' => 'dependetes.store']) !!}
                    <div class="form-row raw-margin-bottom-15">
                        @form_maker_array([
                        'nome' => ['type' => 'string', 'alt_name' => 'Nome'],
                        'email' => ['type' => 'string', 'alt_name' => 'E-mail'],
                        ])
                    </div>
                    <div class="form-row raw-margin-bottom-15">
                        @form_maker_array([
                        'cpf' => ['type' => 'string', 'alt_name' => 'CPF'],
                        'data_nascimento' => ['type' => 'date', 'alt_name' => 'Data de Nascimento'],
                        ])
                    </div>
                    @form_maker_array([
                    'user_id' => ['type' => 'hidden', 'default_value' => auth()->user()->id],
                    ])
                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary pull-right']) !!}
                    <a href="{{ url('dependetes') }}" class="btn btn-secondary"> Voltar</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop