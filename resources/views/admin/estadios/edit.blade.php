@extends('layouts.app')

@section('pageTitle') Estádios &raquo; Editar @stop

@section('content')
<div class="col-md-6 m-auto">
    <div class="card">
        <div class="card-header">
            <div class="col-md-12">
                <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                    {!! Form::open(['route' => 'estadios.search']) !!}
                    <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                    {!! Form::close() !!}
                </div>
                <h1 class="pull-left">Estádios: Editar</h1>
                <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('estadios.create') !!}">Adicionar novo</a>
            </div>
        </div>

        <div class="card-body">
            <div class="col-md-12">
                {!! Form::model($estadio, ['route' => ['estadios.update', $estadio->id], 'method' => 'patch']) !!}

               <div class="form-row raw-margin-bottom-15">
                   @form_maker_object($estadio, FormMaker::getTableColumns('estadios'))
               </div>

                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary pull-right']) !!}
                <a href="{{ url('admin/estadios') }}" class="btn btn-secondary">Voltar</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop
