@extends('layouts.app')

@section('pageTitle') Estádios &raquo; Índice @stop

@section('content')
<div class="col-md-8 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'estadios.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Estádios</h1>
                    <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('estadios.create') !!}">Adicionar novo</a>
                </div>
            </div>

                <div class="card-body">
                    <div class="col-md-12">
                        @if ($estadios->isEmpty())
                            <div class="well text-center">Nenhum estádio encontrado.</div>
                        @else
                            <table class="table table-striped">
                                <thead>
                                    <th>Nome</th>
                                    <th class="text-right" width="200px">Ação</th>
                                </thead>
                                <tbody>
                                    @foreach($estadios as $estadio)
                                        <tr>
                                            <td>
                                                <a href="{!! route('estadios.edit', [$estadio->id]) !!}">{{ $estadio->nome }}</a>
                                            </td>
                                            <td class="text-right">
                                                <form method="post" action="{!! route('estadios.destroy', [$estadio->id]) !!}">
                                                    {!! csrf_field() !!}
                                                    {!! method_field('DELETE') !!}
                                                    <button class="btn btn-danger btn-sm float-right" type="submit" onclick="return confirm('Tem certeza de que deseja excluir este estádio?')"><i class="fa fa-trash"></i> Excluir</button>
                                                </form>
                                                <a class="btn btn-default btn-sm float-right raw-margin-right-16" href="{!! route('estadios.edit', [$estadio->id]) !!}"><i class="fa fa-pencil"></i> Editar</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
           </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {!! $estadios; !!}
        </div>
    </div>

@stop