@extends('layouts.app')

@section('pageTitle') Estádios &raquo; Criar @stop

@section('content')
    <div class="col-md-6 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'estadios.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Estádios: Criar</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">

                    {!! Form::open(['route' => 'estadios.store']) !!}

                   <div class="form-row raw-margin-bottom-15">
                       @form_maker_table("estadios")
                   </div>

                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary pull-right']) !!}
                    <a href="{{ url('admin/estadios') }}" class="btn btn-secondary"> Cancelar</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop