@extends('layouts.app')

@section('pageTitle') Times &raquo; Editar @stop

@section('content')
<div class="col-md-6 m-auto">
    <div class="card">
        <div class="card-header">
            <div class="col-md-12">
                <h1 class="pull-left">Times: Editar</h1>
                <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('times.create') !!}">Adicionar novo</a>
            </div>
        </div>

        <div class="card-body">
            <div class="col-md-12">
                {!! Form::model($time, ['route' => ['times.update', $time->id], 'method' => 'patch']) !!}

                @form_maker_object($time, FormMaker::getTableColumns('times'))

                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary pull-right raw-margin-top-24']) !!}
                <a href="{{ url('admin/times') }}" class="btn btn-secondary raw-margin-top-24">Voltar</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop
