@extends('layouts.app')

@section('pageTitle') Times &raquo; Criar @stop

@section('content')
    <div class="col-md-6 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="float-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'times.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Times: Criar</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">

                    {!! Form::open(['route' => 'times.store']) !!}

                    @form_maker_table("times")

                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary pull-right raw-margin-top-24']) !!}
                    <a href="{{ url('admin/times') }}" class="btn btn-secondary raw-margin-top-24"> Cancelar</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop