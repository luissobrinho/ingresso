@extends('layouts.app')

@section('pageTitle') Clubes &raquo; Índice @stop

@section('content')
<div class="col-md-8 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <h1 class="pull-left">Clubes</h1>
                    <div class="float-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'times.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('times.create') !!}">Adicionar novo</a>
                </div>
            </div>

                <div class="card-body">
                    <div class="col-md-12">
                        @if ($times->isEmpty())
                            <div class="well text-center">Nenhum clube encontrado.</div>
                        @else
                            <table class="table table-striped">
                                <thead>
                                    <th>Nome</th>
                                    <th class="text-right" width="200px">Ação</th>
                                </thead>
                                <tbody>
                                    @foreach($times as $time)
                                        <tr>
                                            <td>
                                                <a href="{!! route('times.edit', [$time->id]) !!}">{{ $time->nome }}</a>
                                            </td>
                                            <td class="text-right">
                                                <form method="post" action="{!! route('times.destroy', [$time->id]) !!}">
                                                    {!! csrf_field() !!}
                                                    {!! method_field('DELETE') !!}
                                                    <button class="btn btn-danger btn-sm float-right" type="submit" onclick="return confirm('Tem certeza de que deseja excluir este time?')"><i class="fa fa-trash"></i> Excluir</button>
                                                </form>
                                                <a class="btn btn-default btn-sm float-right raw-margin-right-16" href="{!! route('times.edit', [$time->id]) !!}"><i class="fa fa-pencil"></i> Editar</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
           </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {!! $times; !!}
        </div>
    </div>

@stop