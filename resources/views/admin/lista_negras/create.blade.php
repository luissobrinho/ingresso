@extends('layouts.app')

@section('pageTitle') Lista Negra &raquo; Criar @stop

@section('content')
    <div class="col-md-6 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'lista_negras.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Lista Negra: Criar</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">

                    {!! Form::open(['route' => 'lista_negras.store']) !!}
                    <div class="form-row raw-margin-bottom-15">
                        @form_maker_array([
                        'cpf' => ['type' => 'string', 'alt_name' => 'CPF'],
                        'user_id' => ['type' => 'relationship', 'alt_name' => 'Usuário', 'model' => 'App\Models\User',
                        'method' => 'getUserCPF'],
                        ])
                    </div>
                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary pull-right']) !!}
                    <a href="{{ url('admin/lista_negras') }}" class="btn btn-secondary"> Voltar</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@stop