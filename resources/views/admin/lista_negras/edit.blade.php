@extends('layouts.app')

@section('pageTitle') Lista Negra &raquo; Editar @stop

@section('content')
<div class="col-md-6 m-auto">
    <div class="card">
        <div class="card-header">
            <div class="col-md-12">
                <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                    {!! Form::open(['route' => 'lista_negras.search']) !!}
                    <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                    {!! Form::close() !!}
                </div>
                <h1 class="pull-left">Lista Negra: Editar</h1>
                <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('lista_negras.create') !!}">Adicionar novo</a>
            </div>
        </div>

        <div class="card-body">
            <div class="col-md-12">
                {!! Form::model($lista_negra, ['route' => ['lista_negras.update', $lista_negra->id], 'method' => 'patch']) !!}

                <div class="form-row raw-margin-bottom-15">
                    @form_maker_array($lista_negra, [
                    'cpf' => ['type' => 'string', 'alt_name' => 'CPF'],
                    'user_id' => ['type' => 'string', 'alt_name' => 'Usuário', 'default_value' => ($lista_negra->user) ? $lista_negra->user->name : '', 'custom' => 'disabled="disabled"'],
                    ])
                </div>

                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary pull-right']) !!}
                <a href="{{ url('admin/lista_negras') }}" class="btn btn-secondary"> Cancelar</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop
