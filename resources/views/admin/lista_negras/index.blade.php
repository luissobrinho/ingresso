@extends('layouts.app')

@section('pageTitle') Lista Negra &raquo; Índice @stop

@section('content')
<div class="col-md-8 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="float-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'lista_negras.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Lista Negra</h1>
                    <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('lista_negras.create') !!}">Adicionar novo</a>
                    <a class="btn btn-secondary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('lista_negras.sync') !!}">Sincronizar</a>
                </div>
            </div>

                <div class="card-body">
                    <div class="col-md-12">
                        @if ($lista_negras->isEmpty())
                            <div class="well text-center">Nenhuma lista negra encontrada.</div>
                        @else
                            <table class="table table-striped">
                                <thead>
                                    <th>Nome</th>
                                    <th class="text-right" width="200px">Ação</th>
                                </thead>
                                <tbody>
                                    @foreach($lista_negras as $lista_negra)
                                        <tr>
                                            <td>
                                                <a href="{!! route('lista_negras.edit', [$lista_negra->id]) !!}">{{ (is_null($lista_negra->cpf)) ? $lista_negra->user->name : $lista_negra->cpf }}</a>
                                            </td>
                                            <td class="text-right">
                                                <form method="post" action="{!! route('lista_negras.destroy', [$lista_negra->id]) !!}">
                                                    {!! csrf_field() !!}
                                                    {!! method_field('DELETE') !!}
                                                    <button class="btn btn-danger btn-sm float-right" type="submit" onclick="return confirm('Você tem certeza que quer deletar essa lista negra?')"><i class="fa fa-trash"></i> Excluir</button>
                                                </form>
                                                <a class="btn btn-default btn-sm float-right raw-margin-right-16" href="{!! route('lista_negras.edit', [$lista_negra->id]) !!}"><i class="fa fa-pencil"></i> Editar</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
           </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {!! $lista_negras; !!}
        </div>
    </div>

@stop