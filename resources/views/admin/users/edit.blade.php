@extends('dashboard')

@section('pageTitle') Users: Edit @stop

@section('content')
    <div class="col-md-8 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    @if (! Session::get('original_user'))
                        <a class="btn btn-secondary pull-right" href="/admin/users/switch/{{ $user->id }}">Login as this User</a>
                    @endif
                    <a class="btn btn-primary pull-right raw-margin-right-10" data-toggle="modal" data-target="#exampleModal" href="#">Document View</a>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <form method="POST" action="/admin/users/{{ $user->id }}">
                        <input name="_method" type="hidden" value="PATCH">
                        {!! csrf_field() !!}

                        <div class="raw-margin-top-24">
                            @input_maker_label('Email')
                            @input_maker_create('email', ['type' => 'string'], $user)
                        </div>

                        <div class="raw-margin-top-24">
                            @input_maker_label('Name')
                            @input_maker_create('name', ['type' => 'string'], $user)
                        </div>

                        <div class="raw-margin-top-24">
                            @input_maker_label('CPF')
                            @input_maker_create('cpf', ['type' => 'string'], $user)
                        </div>

                        @include('user.meta')

                        <div class="raw-margin-top-24">
                            @input_maker_label('Role')
                            @input_maker_create('roles', ['type' => 'relationship', 'model' => 'App\Models\Role', 'label' => 'label', 'value' => 'name'], $user)
                        </div>

                        <div class="raw-margin-top-24">
                            <div class="btn-toolbar justify-content-between">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <a class="btn btn-secondary" href="/admin/users">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Document View</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card raw-margin-top-10">
                        <img src="{{ asset($user->meta->photo) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Photo</h5>
                        </div>
                    </div>
                    <div class="card raw-margin-top-10">
                        <img src="{{ asset($user->meta->identidade) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Document</h5>
                        </div>
                    </div>
                    <div class="card raw-margin-top-10">
                        <img src="{{ asset($user->meta->comprovante) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Comprovant</h5>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@stop