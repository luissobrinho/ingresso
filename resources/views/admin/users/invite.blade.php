@extends('dashboard')

@section('pageTitle') Users: Invite @stop

@section('content')

    <div class="col-md-6 m-auto">
        <div class="card">
            <div class="card-body">
                <div class="col-md-12">
                    <form method="POST" action="/admin/users/invite">
                        {!! csrf_field() !!}
                        <div class="form-row raw-margin-top-24">
                            <div class="col-md">
                                @input_maker_label('Email')
                                @input_maker_create('email', ['type' => 'string'])
                            </div>
                            <div class="col-md">
                                @input_maker_label('Name')
                                @input_maker_create('name', ['type' => 'string'])
                            </div>
                        </div>
                        <div class="form-row raw-margin-top-24">
                            <div class="col-md">
                                @input_maker_label('Role')
                                @input_maker_create('roles', ['type' => 'relationship', 'model' => 'App\Models\Role',
                                'label' => 'label', 'value' => 'name'])
                            </div>

                            <div class="col-md">
                                @input_maker_label('CPF')
                                @input_maker_create('cpf', ['type' => 'string', 'alt_name' => 'CFP'])
                            </div>
                        </div>
                        <div class="form-row raw-margin-top-24">
                            <div class="col-md">
                                @input_maker_label('Data de Nascimento')
                                @input_maker_create('bornDate', ['type' => 'date', 'alt_name' => 'Data de Nascimento'])
                            </div>

                            <div class="col-md">
                                @input_maker_label('Telefone')
                                @input_maker_create('phone', ['type' => 'string', 'alt_name' => 'Telefone'])
                            </div>
                        </div>
                        <div class="raw-margin-top-24">
                            <div class="btn-toolbar justify-content-between">
                                <button class="btn btn-primary" type="submit">Invite</button>
                                <a class="btn btn-secondary" href="/admin/users">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop