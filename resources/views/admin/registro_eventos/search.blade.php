@extends('layouts.app')

@section('pageTitle') Validar Ingresso @stop

@section('content')
<div class="col-md-8 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'admin.registro_eventos.search.view', 'method' => 'get']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

                <div class="card-body">
                    <div class="col-md-12">
                        @if ($registro_eventos->isEmpty())
                            <div class="well text-center">Nenhum registro do evento encontrado.</div>
                        @else
                            <table class="table table-striped">
                                <thead>
                                <th>Nome</th>
                                <th>Evento</th>
                                <th>Status</th>
                                    <th class="text-right" width="300px">Ação</th>
                                </thead>
                                <tbody>
                                    @foreach($registro_eventos as $registro_evento)
                                        <tr>
                                            <td>
                                                @if($registro_evento->pessoa)
                                                    <a href="{!! route('registro_eventos.show', [$registro_evento->id]) !!}">{{ $registro_evento->pessoa->nome }}</a>
                                                @else
                                                    <a href="{!! route('registro_eventos.show', [$registro_evento->id]) !!}">{{ $registro_evento->user->name }}</a>
                                                @endif
                                            </td>
                                            <td>
                                                {{ $registro_evento->evento->nome }} - {{ $registro_evento->evento->time_a->nome }} X {{ $registro_evento->evento->time_b->nome }}
                                            </td>
                                            <td>
                                                @if($registro_evento->status)
                                                    Pagamento aprovado
                                                @else
                                                    Aguardando confirmação de pagamento
                                                @endif
                                            </td>
                                            <td class="text-right">
                                                <a class="btn btn-default btn-xs float-right raw-margin-right-16" href="{!! route('registro_eventos.show', [$registro_evento->id]) !!}"><i class="fa fa-eye"></i> Ver</a>
                                                @if(! $registro_evento->status)
                                                    <form method="post" action="{!! route('admin.registro_eventos.update', ['id' => $registro_evento->id]) !!}">
                                                        {!! csrf_field() !!}
                                                        {!! method_field('PUT') !!}
                                                        @input_maker_create('status', ['type' => 'hidden', 'default_value' => '1'])
                                                        <button class="btn btn-success btn-sm float-right" type="submit" onclick="return confirm('Deseja confimar esse pagamento?')"><i class="fa fa-money"></i> Pago</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
           </div>
    </div>

@stop