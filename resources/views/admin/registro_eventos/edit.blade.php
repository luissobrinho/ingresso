@extends('layouts.app')

@section('pageTitle') RegistroEventos &raquo; Editar @stop

@section('content')
<div class="col-md-6 m-auto">
    <div class="card">
        <div class="card-header">
            <div class="col-md-12">
                <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                    {!! Form::open(['route' => 'registro_eventos.search']) !!}
                    <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                    {!! Form::close() !!}
                </div>
                <h1 class="pull-left">RegistroEventos: Editar</h1>
                <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('registro_eventos.create') !!}">Adicionar novo</a>
            </div>
        </div>

        <div class="card-body">
            <div class="col-md-12">
                {!! Form::model($registro_evento, ['route' => ['registro_eventos.update', $registro_evento->id], 'method' => 'patch']) !!}

                @form_maker_object($registro_evento, FormMaker::getTableColumns('registro_eventos'))

                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary pull-right']) !!}
                <a href="{{ url('admin/registro_eventos') }}" class="btn btn-secondary"> Cancelar</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop
