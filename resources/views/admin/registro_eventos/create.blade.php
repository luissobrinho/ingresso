@extends('layouts.app')

@section('pageTitle') Registro de Evento &raquo; Criar @stop

@section('content')
    <div class="col-md-6 m-auto">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                        {!! Form::open(['route' => 'registro_eventos.search']) !!}
                        <input class="form-control form-inline pull-right" name="search" placeholder="Procurar">
                        {!! Form::close() !!}
                    </div>
                    <h1 class="pull-left">Registro de Evento: Criar</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">

                    {!! Form::open(['route' => 'admin.registro_eventos.store']) !!}

                   <div class="form-row raw-margin-bottom-15">
                       @form_maker_array([
                       'tipo' => ['type' => 'select', 'alt_name' => 'Tipo', 'options' => ['Usuário' => 0, 'Pessoa' => 1]],
                       'assento' => ['type' => 'select', 'alt_name' => 'Assento', 'options' => ['Alto' => 0, 'Baixo' => 1, 'Especial' => 2]],
                       'evento_id' => [
                       'type' => 'relationship',
                       'alt_name' => 'Evento',
                       'model' => 'App\Models\Evento',
                       'label' => 'nome',
                       'method' => 'EventValid'
                       ]
                       ])
                   </div>

                    <div class="form-row user raw-margin-bottom-15">
                        @form_maker_array([
                        'user_id' => [
                        'type' => 'relationship',
                        'alt_name' => 'Usuário',
                        'model' => 'App\Models\User',
                        'method' => 'getUserCPF'
                        ]
                        ])
                    </div>

                    <div class="form-row pessoa raw-margin-bottom-15" style="display: none">
                        @form_maker_array([
                        'nome' => ['type' => 'string', 'alt_name' => 'Nome'],
                        'email' => ['type' => 'string', 'alt_name' => 'E-mail'],
                        'cpf' => ['type' => 'string', 'alt_name' => 'CPF'],
                        ])
                    </div>

                    {!! Form::submit('Salvar', ['class' => 'btn btn-primary pull-right']) !!}
                    <a href="{{ url('admin/registro_eventos') }}" class="btn btn-secondary"> Voltar</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(document).on('change', '#Tipo', function (evt) {
                if($(evt.currentTarget).val() == 0) {
                    $('.user').show();
                    $('.pessoa').hide().find('input').val('');
                } else {
                    $('.pessoa').show();
                    $('.user').hide().find('select').val('');
                }
            })
        })
    </script>
@stop