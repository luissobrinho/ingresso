@extends('layouts.app')

@section('pageTitle') Eventos &raquo; Editar @stop

@section('content')
<div class="col-md-8 m-auto">
    <div class="card">
        <div class="card-header">
            <div class="col-md-12">
                <div class="float-right raw-margin-top-24 raw-margin-left-24 col-md-4">
                    <img src="{{ url($evento->foto) }}" alt="" class="float-right w-100">
                </div>
                <h1 class="pull-left">Eventos: Editar</h1>
            </div>
        </div>

        <div class="card-body">
            <div class="col-md-12">
                {!! Form::model($evento, ['route' => ['admin.eventos.update', $evento->id], 'method' => 'patch', 'files' => true]) !!}

                <div class="form-row raw-margin-bottom-15">
                    @form_maker_array($evento, [
                    'nome' => ['type' => 'string'],
                    'data_evento' => ['type' => 'date', 'alt_name' => 'Data do Evento'],
                    ])
                </div>

                <div class="form-row raw-margin-bottom-15">
                    @form_maker_array($evento, [
                    'time_1_id' => ['type' => 'relationship', 'alt_name' => 'Time A', 'model' => 'App\Models\Time', 'label' => 'nome'],
                    'time_2_id' => ['type' => 'relationship', 'alt_name' => 'Time B', 'model' => 'App\Models\Time', 'label' => 'nome'],
                    'estadio_id' => ['type' => 'relationship', 'alt_name' => 'Estádio', 'model' => 'App\Models\Estadio', 'label' => 'nome'],
                    ])
                </div>

                <div class="form-row raw-margin-bottom-15">
                    @form_maker_array($evento, [
                    'quantidade_alta' => ['type' => 'number', 'alt_name' => 'Quantidade Alta'],
                    'quantidade_baixa' => ['type' => 'number', 'alt_name' => 'Quantidade Baixa'],
                    'quantidade_especial' => ['type' => 'number', 'alt_name' => 'Quantidade Especial'],
                    ])
                </div>

                <div class="form-row raw-margin-bottom-15">
                    @form_maker_array($evento, [
                    'quantidade_alta_valor' => ['type' => 'string', 'alt_name' => 'Valor Quantidade Alta'],
                    'quantidade_baixa_valor' => ['type' => 'string', 'alt_name' => 'Valor Quantidade Baixa'],
                    'quantidade_especial_valor' => ['type' => 'string', 'alt_name' => 'Valor Quantidade Especial'],
                    ])
                </div>

                <div class="form-row raw-margin-bottom-15">
                    @form_maker_array($evento, [
                    'descricao' => ['type' => 'text', 'alt_name' => 'Descriçao'],
                    'arquivo' => ['type' => 'file', 'alt_name' => 'Foto'],
                    ])
                </div>

                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary pull-right']) !!}
                <a href="{{ url('admin/eventos') }}" class="btn btn-secondary">Voltar</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop
