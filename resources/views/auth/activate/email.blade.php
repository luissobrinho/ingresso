@extends('layouts.app')

@section('pageTitle')Activate @stop

@section('content')

    <div class="row">
        <div class="col-md-12 form-small text-center">

            <h1 class="text-center">Activate</h1>

            <p>Please wait for confirmation of activation of your account in the email.</p>

            {{--<a class="btn btn-primary" href="{{ url('activate/send-token') }}">Request new Token</a>--}}
        </div>
    </div>

@stop

