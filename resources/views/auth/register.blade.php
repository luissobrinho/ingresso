@extends('layouts.app')

@section('pageTitle') Register @stop

@section('content')
    <div class="col-md-8 m-auto">
        <div class="card">
            <div class="card-header">
                <h2 class="text-center">Register</h2>
            </div>
            <div class="card-body">

                <div class="form-small">

                    <form method="POST" action="/register" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-row">
                            <div class="col-md raw-margin-top-24">
                                <label>Name</label>
                                <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Name">
                            </div>
                            <div class="col-md raw-margin-top-24">
                                <label>Email</label>
                                <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Email">
                            </div>
                        </div>
                            <div class="col-md">
                                @input_maker_create('roles', ['type' => 'hidden', 'default_value' => 'member'])
                            </div>
                        <div class="form-row raw-margin-top-24">
                            <div class="col-md">
                                @input_maker_label('CPF')
                                @input_maker_create('cpf', ['type' => 'string', 'alt_name' => 'CFP'])
                            </div>
                        </div>
                        <div class="form-row raw-margin-top-24">
                            <div class="col-md">
                                @input_maker_label('Data de Nascimento')
                                @input_maker_create('bornDate', ['type' => 'date', 'alt_name' => 'Data de Nascimento'])
                            </div>

                            <div class="col-md">
                                @input_maker_label('Telefone')
                                @input_maker_create('phone', ['type' => 'string', 'alt_name' => 'Telefone', 'custom' => 'data-mask="(00) 00000-0000"'])
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md raw-margin-top-24">
                                <label for="foto" class="form-control">Foto de perfil</label>
                                <input style="display: none;" type="file" name="arquivo_photo" id="foto" required>
                            </div>
                            <div class="col-md raw-margin-top-24">
                                <label for="rg" class="form-control">Carteira de identidade</label>
                                <input style="display: none;" type="file" name="arquivo_identidade" id="rg" required>
                            </div>
                            <div class="col-md raw-margin-top-24">
                                <label for="conprovante" class="form-control">Comprovante de residência</label>
                                <input style="display: none;" type="file" name="arquivo_conprovante" id="conprovante" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md raw-margin-top-24">
                                <label>Password</label>
                                <input class="form-control" type="password" name="password" placeholder="Password">
                            </div>
                            <div class="col-md raw-margin-top-24">
                                <label>Confirm Password</label>
                                <input class="form-control" type="password" name="password_confirmation" placeholder="Password Confirmation">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 raw-margin-top-24">
                                <div class="btn-toolbar justify-content-between">
                                    <button class="btn btn-primary" type="submit">Register</button>
                                    <a class="btn btn-link" href="/login">Login</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@stop