<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a given Closure or controller and enjoy the fresh air.
|
*/

/*
|--------------------------------------------------------------------------
| Welcome Page
|--------------------------------------------------------------------------
*/

Route::get('/', 'PagesController@home');

/*
|--------------------------------------------------------------------------
| Login/ Logout/ Password
|--------------------------------------------------------------------------
*/
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/*
|--------------------------------------------------------------------------
| Registration & Activation
|--------------------------------------------------------------------------
*/
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('activate/token/{token}', 'Auth\ActivateController@activate');
Route::group(['middleware' => ['auth']], function () {
    Route::get('activate', 'Auth\ActivateController@showActivate');
    Route::get('activate/send-token', 'Auth\ActivateController@sendToken');
});

/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['auth', 'active']], function () {

    /*
    |--------------------------------------------------------------------------
    | General
    |--------------------------------------------------------------------------
    */

    Route::get('/users/switch-back', 'Admin\UserController@switchUserBack');

    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
        Route::get('settings', 'SettingsController@settings');
        Route::post('settings', 'SettingsController@update');
        Route::get('password', 'PasswordController@password');
        Route::post('password', 'PasswordController@update');
    });

    /*
    |--------------------------------------------------------------------------
    | Dashboard
    |--------------------------------------------------------------------------
    */

    Route::get('/dashboard', 'PagesController@dashboard');

    /*
    |--------------------------------------------------------------------------
    | Team Routes
    |--------------------------------------------------------------------------
    */

    Route::get('team/{name}', 'TeamController@showByName');
    Route::resource('teams', 'TeamController', ['except' => ['show']]);
    Route::post('teams/search', 'TeamController@search');
    Route::post('teams/{id}/invite', 'TeamController@inviteMember');
    Route::get('teams/{id}/remove/{userId}', 'TeamController@removeMember');

    /*
    |--------------------------------------------------------------------------
    | Admin
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');

        /*
        |--------------------------------------------------------------------------
        | Users
        |--------------------------------------------------------------------------
        */
        Route::resource('users', 'UserController', ['except' => ['create', 'show']]);
        Route::post('users/search', 'UserController@search');
        Route::get('users/search', 'UserController@index');
        Route::get('users/invite', 'UserController@getInvite');
        Route::get('users/switch/{id}', 'UserController@switchToUser');
        Route::post('users/invite', 'UserController@postInvite');

        /*
        |--------------------------------------------------------------------------
        | Roles
        |--------------------------------------------------------------------------
        */
        Route::resource('roles', 'RoleController', ['except' => ['show']]);
        Route::post('roles/search', 'RoleController@search');
        Route::get('roles/search', 'RoleController@index');

        /*
        |--------------------------------------------------------------------------
        | Time Routes
        |--------------------------------------------------------------------------
        */
        Route::resource('times', 'TimesController', ['except' => ['show']]);
        Route::post('times/search', [
            'as' => 'times.search',
            'uses' => 'TimesController@search'
        ]);

        /*
        |--------------------------------------------------------------------------
        | Evento Routes
        |--------------------------------------------------------------------------
        */
        Route::resource('eventos', 'EventosController', ['except' => ['show']])->names('admin.eventos');
        Route::get('eventos/{id}/copy', 'EventosController@copy')->name('admin.eventos.copy');
        Route::post('eventos/search', [
            'as' => 'admin.eventos.search',
            'uses' => 'EventosController@search'
        ]);

        /*
        |--------------------------------------------------------------------------
        | ListaNegra Routes
        |--------------------------------------------------------------------------
        */
        Route::resource('lista_negras', 'ListaNegrasController', ['except' => ['show']]);
        Route::get('lista_negras/server/sync', 'ListaNegrasController@sync')->name('lista_negras.sync');
        Route::post('lista_negras/search', [
            'as' => 'lista_negras.search',
            'uses' => 'ListaNegrasController@search'
        ]);

        /*
        |--------------------------------------------------------------------------
        | RegistroEvento Routes
        |--------------------------------------------------------------------------
        */
        Route::resource('registro_eventos', 'RegistroEventosController', ['except' => ['destroy']])->names('admin.registro_eventos');
        Route::post('registro_eventos/search', [
            'as' => 'admin.registro_eventos.search',
            'uses' => 'RegistroEventosController@search'
        ]);
        Route::get('registro_eventos/valid/ingress/valid-ingress', [
            'as' => 'admin.registro_eventos.search.view',
            'uses' => 'RegistroEventosController@searchView'
        ]);


        /*
        |--------------------------------------------------------------------------
        | Estadio Routes
        |--------------------------------------------------------------------------
        */
        Route::resource('estadios', 'EstadiosController', ['except' => ['show']]);
        Route::post('estadios/search', [
            'as' => 'estadios.search',
            'uses' => 'EstadiosController@search'
        ]);
    });

    /*
    |--------------------------------------------------------------------------
    | RegistroEvento Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('registro_eventos', 'RegistroEventosController');
    Route::get('registro_eventos', 'RegistroEventosController@index')->name('pagseguro.redirect');
    Route::post('registro_eventos/search', [
        'as' => 'registro_eventos.search',
        'uses' => 'RegistroEventosController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Dependete Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('dependetes', 'DependetesController', ['except' => ['show']]);
    Route::post('dependetes/search', [
        'as' => 'dependetes.search',
        'uses' => 'DependetesController@search'
    ]);




});

/*
|--------------------------------------------------------------------------
| Evento Routes
|--------------------------------------------------------------------------
*/
Route::resource('eventos', 'EventosController', ['only' => ['show']]);
Route::post('eventos/search', [
    'as' => 'eventos.search',
    'uses' => 'EventosController@search'
]);

Route::post('/pagseguro/notification', [
    'uses' => '\laravel\pagseguro\Platform\Laravel5\NotificationController@notification',
    'as' => 'pagseguro.notification',
]);
